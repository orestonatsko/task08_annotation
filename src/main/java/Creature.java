import annotations.Extract;
import annotations.MethodDesc;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Creature {

    public Creature(String name, boolean isMammal, boolean isFriendly) {
        this.name = name;
        this.isFriendly = isFriendly;
        this.isMammal = isMammal;
    }

    @Extract
    private
    String name;

    private char gender;

    private String[] bodyParts;
    @Extract
    private boolean isMammal;

    @Extract
    private boolean isFriendly;

    public void move() {
        System.out.println("Moving");
    }

    @MethodDesc(name = "sleep", param = "location")
    public void sleep(String location) {
        System.out.println("Sleeping on " + location);
    }

    @MethodDesc(name = "eat", param = "Food")
    public void eat(String food) {
        System.out.println("eating " + food);
    }

    static void getValuesFromMethodDesc() throws ClassNotFoundException {
        Class<?> clazz = Class.forName("Creature");
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            MethodDesc a = method.getAnnotation(MethodDesc.class);
            if(a != null){
                System.out.println( "@MethodDesc(name=" + a.name() + ", param= " + a.param() + ")");
            }
        }
    }

    static void getFieldsWithoutInstance() throws ClassNotFoundException {
        Class<?> clazz = Class.forName("Creature");
        Field[] fields = clazz.getDeclaredFields();
        System.out.println("Extracted fields from Creature.class:");
        for (Field field : fields) {
            if (field.isAnnotationPresent(Extract.class)) {
                System.out.println(field.getName());
            }

        }
    }
}
