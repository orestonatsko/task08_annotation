package menu;

public abstract class MenuItem {
    private String name;

    public MenuItem(String name) {
        this.name = name;
    }

    public abstract void execute();

    public String getName(){
        return name;
    }
}
