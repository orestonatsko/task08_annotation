import menu.Menu;
import menu.MenuItem;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class App {
    private String unknownField = "unknown";

    public static void main(String[] args) {
        Menu menu = Menu.getInstance();
        initializeMenu(menu);
        menu.show();
    }

    private static void initializeMenu(Menu menu) {
        menu.addMenuItem(new MenuItem("Get fields with custom annotation") {
            @Override
            public void execute() {
                try {
                    Creature.getFieldsWithoutInstance();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

        menu.addMenuItem(new MenuItem("Get values of @MethodDesc annotation") {
            @Override
            public void execute() {
                try {
                    Creature.getValuesFromMethodDesc();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
        menu.addMenuItem(new MenuItem("Invoke methods from Operations class") {
            @Override
            public void execute() {
                Operations.executeAllMethods();
            }
        });
        menu.addMenuItem(new MenuItem("Change value") {
            @Override
            public void execute() {
                new App().changeUnknownValue();
            }
        });
        menu.addMenuItem(new MenuItem("Invoke private methods with varargs parameters") {
            @Override
            public void execute() {
                new Operations().invokeMyMethods();
            }
        });
        menu.addMenuItem(new MenuItem("Get information about unknown class") {
            @Override
            public void execute() {
                List<Object> objects = Arrays.asList("Word", 5, false, 3.6);
                int min = 1;
                int max = 4;
                Random r = new Random();
                Object o = objects.get(r.ints(min, (max)).limit(1).findFirst().getAsInt());
                new ClassViewer(o).showInfo();
            }
        });
    }

    private void changeUnknownValue() {
        try {
            Class clazz = this.getClass();
            Field field = clazz.getDeclaredField("unknownField");
            Object newValue = " App field is changed";
            field.set(this, newValue);
            System.out.println("unknownField: " + this.unknownField);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
