import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class Operations {




    private int plus(int a, int b) {
        System.out.print("plus method: ");
        return a + b;
    }

    private boolean doubleTrue(boolean a, boolean b) {
        System.out.print("doubleTrue method: ");
        return a && b;
    }

    String concat(String s1, String s2) {
        System.out.print("concat method: ");
        return s1 + " " + s2;
    }

   private void myMethod(String s, int... args) {
        int sum = 0;
        for (int arg : args) {
            sum += arg;
        }
        System.out.println(s + " sum of arguments is " + sum);

    }

   private void myMethod(String... args) {
        StringBuilder sentence = new StringBuilder();
        for (String arg : args) {
            sentence.append(arg);
            sentence.append(" ");
        }
        System.out.println("myMethod2: " + sentence);

    }
      void invokeMyMethods() {
        Class<?> clazz = this.getClass();
          try {
              Method myMethod1 = clazz.getDeclaredMethod("myMethod", String.class, int[].class);
              Method myMethod2 = clazz.getDeclaredMethod("myMethod", String[].class);
              myMethod1.invoke(this, "myMethod2: ", new int[]{1, 2, 3});
              myMethod2.invoke(this, (Object) new String[]{"one", "two", "three"});
          } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
              e.printStackTrace();
          }

      }

    static void executeAllMethods() {
        try {
            Class<?> clazz = Class.forName("Operations");
            Method[] methods = clazz.getDeclaredMethods();
            Object o = clazz.getDeclaredConstructor().newInstance();
            for (Method method : methods) {
                switch (method.getName()) {
                    case "plus":
                        int res = (int) method.invoke(o, 2, 2);
                        System.out.println("2 + 2= " + res);
                        break;
                    case "doubleTrue":
                        boolean doubleTrue = (boolean) method.invoke(o, true, false);
                        System.out.println("DoubleTrue - " + doubleTrue);
                        break;
                    case "concat":
                        String concatenated = (String) method.invoke(o, "Hello", "all!");
                        System.out.println("Concated string is " + concatenated);
                        break;
                }
            }
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException
                | NoSuchMethodException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}

