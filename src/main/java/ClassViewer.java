import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

 class ClassViewer {
   private Object instance;

     ClassViewer(Object instance) {
        this.instance = instance;
    }

    void showInfo() {
        Class<?> clazz = instance.getClass();
        String sipleName = clazz.getSimpleName();
        Field[] fields = clazz.getDeclaredFields();
        Method[] methods = clazz.getDeclaredMethods();
        Constructor[] constructors = clazz.getDeclaredConstructors();
        String packageName = clazz.getPackageName();
        System.out.println("Package name: " + packageName + "; class name: " + sipleName);
        System.out.println();
        System.out.println("Constructors: " + constructors.length);
        for (Constructor constructor : constructors) {
            System.out.print(constructor.getName() + ": ");
            for(Class cls : constructor.getParameterTypes()){
                System.out.print("Parameters: " + cls.getSimpleName() + ", ");
            }
            System.out.println();
        }
        System.out.println();
        System.out.println("Fields: ");
        for (Field field : fields) {
            System.out.print(field.getName() + ", ");
        }
        System.out.println();
        System.out.println("Methods:");
        for (Method method : methods) {
            System.out.print(method.getName() + ", ");
        }


    }
}
